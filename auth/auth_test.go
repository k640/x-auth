package auth

import (
	"crypto/rand"
	"crypto/rsa"
	"testing"

	"github.com/golang-jwt/jwt"
	"github.com/stretchr/testify/assert"
)

func TestCreateToken(t *testing.T) {
	private, _ := rsa.GenerateKey(rand.Reader, 3072)
	public := &private.PublicKey
	auth := NewAuthService(private, public)
	tokenString := auth.CreateToken(jwt.MapClaims{"id": 1})

	token, _ := jwt.Parse(tokenString, func(t *jwt.Token) (interface{}, error) {
		return public, nil
	})
	assert.True(t, token.Valid)
}
