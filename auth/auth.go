package auth

import (
	"crypto/rsa"
	"fmt"

	"github.com/golang-jwt/jwt"
)

type UserService interface {
	RetrieveByCredentials(email string, password string) (int64, error)
}

type AuthService interface {
	CreateToken(data jwt.MapClaims) string
}

type authService struct {
	privateKey *rsa.PrivateKey
	publicKey  *rsa.PublicKey
}

func NewAuthService(privateKey *rsa.PrivateKey, publicKey *rsa.PublicKey) AuthService {
	return &authService{
		privateKey: privateKey,
		publicKey:  publicKey,
	}
}

func (a authService) CreateToken(data jwt.MapClaims) string {
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, data)
	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString(a.privateKey)
	if err != nil {
		fmt.Printf("signed error %v", err)
	}
	return tokenString
}
