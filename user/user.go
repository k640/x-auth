package user

import (
	"net/http"

	"github.com/go-resty/resty/v2"
)

type UserService struct {
	c   *resty.Client
	url string
}

func NewUserService(c *resty.Client, url string) *UserService {
	return &UserService{
		c,
		url,
	}

}

type retrieveByCredentialsReposnce struct {
	Id uint64 `json:"user_id"`
}

func (u *UserService) RetrieveByCredentials(email string, password string) (uint64, error) {
	id := retrieveByCredentialsReposnce{}
	resp, err := u.c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Accept", "application/json").
		SetBody(`{"email":"` + email + `", "password": "` + password + `"}`).
		SetResult(&id).
		Post("http://" + u.url + "/get-by-credential")
	if resp.StatusCode() != http.StatusOK {
		return 0, err
	}
	return id.Id, nil
}
