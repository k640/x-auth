FROM golang:1.18-buster AS build

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . /app
RUN ls -la /

RUN go build -o /build ./cmd/api/


##
## Deploy
##
FROM gcr.io/distroless/base-debian10

WORKDIR /

COPY --from=build /build /build


EXPOSE 8010

USER nonroot:nonroot

ENTRYPOINT ["/build"]