package main

import (
	"crypto/rsa"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
	"github.com/golang-jwt/jwt"
	"gitlab.com/k640/x-auth/auth"
	"gitlab.com/k640/x-auth/user"
)

type Credentials struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
}

func retrieveRSAKeys() (*rsa.PrivateKey, *rsa.PublicKey) {
	signBytes, err := ioutil.ReadFile("./certs/private.pem")

	if err != nil {
		panic(err)
	}

	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)

	if err != nil {
		panic(err)
	}

	verifyBytes, err := ioutil.ReadFile("./certs/public.pem")

	if err != nil {
		panic(err)
	}

	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(verifyBytes)

	if err != nil {
		panic(err)
	}
	return privateKey, publicKey
}

func main() {
	r := gin.Default()
	authService := auth.NewAuthService(retrieveRSAKeys())
	client := resty.New()
	user_host := "localhost:1111"
	if value, present := os.LookupEnv("USER_HOST"); present {
		user_host = value
	}

	userService := user.NewUserService(client, user_host)
	r.POST("/auth/authenticate", func(c *gin.Context) {
		credentials := Credentials{}
		if err := c.Bind(&credentials); err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{
				"error": "invalid credentials",
			})
			fmt.Print(err)
			return
		}

		id, _ := userService.RetrieveByCredentials(credentials.Email, credentials.Password)
		if id == 0 {
			c.JSON(http.StatusUnauthorized, gin.H{
				"error": "invalid credentials",
			})
			return
		}
		tokenString := authService.CreateToken(jwt.MapClaims{
			"id": id,
		})
		c.JSON(http.StatusOK, gin.H{
			"token": tokenString,
		})
	})

	r.GET("/auth/public-key", func(c *gin.Context) {
		c.File("./certs/public.pem")
	})
	if value, present := os.LookupEnv("APP_PORT"); present {
		r.Run(":" + value)
	} else {
		r.Run(":8003")
	}

}
